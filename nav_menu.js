function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
  if (!event.target.matches('.menu')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

document.addEventListener("DOMContentLoaded", function() {
  var delayImg = document.querySelectorAll("img.delay");
  var loadDelayTimeout;

  function loadDelay () {
    if(loadDelayTimeout) {
      clearTimeout(loadDelayTimeout);
    }

    loadDelayTimeout = setTimeout(function() {
        var scrollTop = window.pageYOffset;
        delayImg.forEach(function(img) {
            if(img.offsetTop < (window.innerHeight + scrollTop)) {
              img.src = img.dataset.src;
              img.classList.remove('delay');
            }
        });
        if(delayImg.length == 0) {
          document.removeEventListener("scroll", loadDelay);
          window.removeEventListener("resize", loadDelay);
          window.removeEventListener("orientationChange", loadDelay);
        }
    }, 20);
  }

  document.addEventListener("scroll", loadDelay);
  window.addEventListener("resize", loadDelay);
  window.addEventListener("orientationChange", loadDelay);
});
